# Choose Your Music Kiosk

The best and most collaberative music player ever.

Further documentation is being written in the `docs/` folder.

## Getting Started

### Language Requirements

This project has been tested with *Ruby 2.3.1* and *Node 6.4.0*.

### Dependencies

* redis
* sqlite3
* imagemagick (`convert` is required for cover resizing)

### Install and Run

```
$ gem install bundler
$ gem install foreman
$ bundle install
$ npm install
$ foreman start
```
