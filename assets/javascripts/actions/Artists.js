import { Artists as ArtistActionTypes } from '../constants/actions';
import client from '../client';

export function getAll() {
  return {
    type:    ArtistActionTypes.getAll,
    payload: client.get('/data/artists.json')
  };
}

export function select(id) {
  return {
    type:    ArtistActionTypes.select,
    payload: parseInt(id)
  };
}
