import { Status as StatusActionTypes } from '../constants/actions';
import client from '../client';

export function connected() {
  return {
    type: StatusActionTypes.connected
  };
}

export function disconnected() {
  return {
    type: StatusActionTypes.disconnected
  };
}

export function playing(payload) {
  return {
    payload,
    type: StatusActionTypes.playing
  };
}

export function stopped() {
  return {
    type: StatusActionTypes.stopped
  };
}

export function progress(payload) {
  return {
    payload,
    type: StatusActionTypes.progress,
    meta: { silent: true }
  };
}
