import { Genres as GenreActionTypes } from '../constants/actions';
import client from '../client';

export function getAll() {
  return {
    type:    GenreActionTypes.getAll,
    payload: client.get('/data/genres.json')
  };
}
