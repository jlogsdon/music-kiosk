import { Albums as AlbumActionTypes } from '../constants/actions';
import client from '../client';

export function getAll() {
  return {
    type:    AlbumActionTypes.getAll,
    payload: client.get('/data/albums.json')
  }
}

export function select(id) {
  return {
    type:    AlbumActionTypes.select,
    payload: parseInt(id)
  };
}
