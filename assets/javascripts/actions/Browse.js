import { Browse as BrowseActionTypes } from '../constants/actions';
import client from '../client';

const MAX_QUEUE_ITEMS = 10;

export function search(term) {
  return {
    type:    BrowseActionTypes.search,
    payload: term
  };
}

export function add(id) {
  return getState => {
    const { queue } = getState().Browse;

    if (queue.length >= MAX_QUEUE_ITEMS) {
      return;
    }

    return {
      type:    BrowseActionTypes.setQueue,
      payload: queue.concat(id)
    };
  };
}

export function remove(id) {
  return getState => ({
    type:    BrowseActionTypes.setQueue,
    payload: getState().Browse.queue.filter(i => i !== id)
  });
}

export function save() {
  return getState => ({
    type:    BrowseActionTypes.save,
    payload: client.post('/api/queue', {
      data: {
        ids: getState().Browse.queue
      }
    })
  });
}

export function clearQueue() {
  return {
    type: BrowseActionTypes.clearQueue
  };
}

export function getQueue() {
  return {
    type:    BrowseActionTypes.getQueue,
    payload: client.get('/api/queue')
  };
}

function requireAdmin(actionCreator) {
  return getState => {
    const { isAdmin } = getState().Admin;

    if (isAdmin) {
      return actionCreator(getState);
    }
  };
}

export function queueEntireAlbum(id) {
  return requireAdmin(getState => {
    const { albums } = getState().Albums;
    const album = albums[id];

    if (!album) {
      return;
    }

    return {
      type:    BrowseActionTypes.save,
      payload: client.post('/api/queue', {
        data: {
          ids: album.tracks
        }
      })
    };
  });
}
export function playNow() {
  return requireAdmin(getState => {
    return {
      type:    BrowseActionTypes.playNow,
      payload: client.post('/api/admin/play', {
        data: {
          ids: getState().Browse.queue
        }
      })
    };
  });
}
