import { Tracks as TrackActionTypes } from '../constants/actions';
import client from '../client';

export function getAll() {
  return {
    type:    TrackActionTypes.getAll,
    payload: client.get('/data/tracks.json')
  }
}
