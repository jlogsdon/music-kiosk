import { bindActionCreators } from 'redux';
import store from '../store';
import * as AlbumActions from './Albums';
import * as ArtistActions from './Artists';
import * as TrackActions from './Tracks';
import * as StatusActions from './Status';
import * as BrowseActions from './Browse';
import * as AdminActions from './Admin';
import * as GenreActions from './Genres';

const dispatch = store.dispatch;
module.exports = {
  AlbumActions: bindActionCreators(AlbumActions, dispatch),
  ArtistActions: bindActionCreators(ArtistActions, dispatch),
  TrackActions: bindActionCreators(TrackActions, dispatch),
  StatusActions: bindActionCreators(StatusActions, dispatch),
  BrowseActions: bindActionCreators(BrowseActions, dispatch),
  AdminActions: bindActionCreators(AdminActions, dispatch),
  GenreActions: bindActionCreators(GenreActions, dispatch),
};
