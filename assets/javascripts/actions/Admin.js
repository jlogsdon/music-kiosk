import { Admin as AdminActionTypes } from '../constants/actions';
import { pushState } from 'redux-react-router';
import client from '../client';

const passwords = {
  'long': 60 * 60 * 1000,
  'cymk': 60 * 1000,
  'blah': 5 * 1000
};

export function attemptToLogin(password) {
  return (getState, dispatch) => {
    const { isAdmin } = getState().Admin;
    const timeToLive  = passwords[password];

    if (isAdmin || !timeToLive) {
      return;
    }

    const payload = setTimeout(() => dispatch(logout()), timeToLive);

    return {
      type: AdminActionTypes.login,
      payload
    };
  };
}

export function logout() {
  return (getState, dispatch) => {
    const { timeout } = getState().Admin;

    clearTimeout(timeout);

    return {
      type: AdminActionTypes.logout
    };
  };
}

function requireAdmin(actionCreator) {
  return getState => {
    const { isAdmin } = getState().Admin;

    if (isAdmin) {
      return actionCreator();
    }
  };
}

export function playNow(track_id) {
  return requireAdmin(() => ({
    type:    AdminActionTypes.playNow,
    payload: client.post(`/api/admin/play`, {
      data: {
        id: track_id
      }
    })
  }));
}
export function rocket(selection_id) {
  return requireAdmin(() => ({
    type:    AdminActionTypes.rocket,
    payload: client.post(`/api/admin/queue/${selection_id}/play`)
  }));
}
export function bumpUp(selection_id) {
  return requireAdmin(() => ({
    type:    AdminActionTypes.bumpUp,
    payload: client.post(`/api/admin/queue/${selection_id}/up`)
  }));
}
export function bumpDown(selection_id) {
  return requireAdmin(() => ({
    type:    AdminActionTypes.bumpDown,
    payload: client.post(`/api/admin/queue/${selection_id}/down`)
  }));
}
export function remove(selection_id) {
  return requireAdmin(() => ({
    type:    AdminActionTypes.remove,
    payload: client.del(`/api/admin/queue/${selection_id}`)
  }));
}
export function skip(skip) {
  return requireAdmin(() => ({
    type:    AdminActionTypes.skip,
    payload: client.post('/api/admin/skip')
  }));
}

export function fetchAccessLists() {
  return requireAdmin(() => ({
    type:    AdminActionTypes.fetchAccessLists,
    payload: client.get('/api/admin/access_lists')
  }));
}
export function saveAccessList(accessList) {
  return requireAdmin(() => ({
    type:    AdminActionTypes.saveAccessList,
    payload: client.post('/api/admin/access_lists', {
      data: accessList
    })
  }));
}
export function loadAccessList(accessListID) {
  return requireAdmin(() => ({
    type:    AdminActionTypes.loadAccessList,
    payload: client.post(`/api/admin/access_lists/${accessListID}/load`)
  }));
}
export function clearAccessList() {
  return requireAdmin(() => ({
    type:    AdminActionTypes.clearAccessList,
    payload: client.post('/api/admin/access_lists/clear')
  }));
}
