export function expandTrackFromID(id, { Tracks, Albums, Artists }) {
  const track  = Tracks.tracks[id];
  const album  = track && Albums.albums[track.album_id];
  const artist = track && Artists.artists[track.artist_id];
  const cover  = `http://localhost:9292/cover/${album ? album.id : 0}`;

  return {
    album, artist,
    track: { cover, ...track }
  };
}

export function nowPlaying({ Status, ...state }) {
  return {
    ...expandTrackFromID(Status.track, state),
    status:   Status.state,
    position: Status.position
  };
}

export function upNext({ Browse, ...state }) {
  return Browse.persisted.map(item => ({
    ...item,
    ...expandTrackFromID(item.track_id, state)
  }));
}
