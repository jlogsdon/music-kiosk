import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ReduxRouter } from 'redux-react-router';
import routes from './routes';
import store from './store';
import socket from './socket';
import client from './client';
import {
  AlbumActionTypes,
  ArtistActionTypes,
  TrackActionTypes,
  GenreActionTypes
} from './actions';

client.get('/data.json').then(res => {
  const dispatch = (type, name, data) =>
    store.dispatch({
      type: type,
      payload: {
        [name]: data.records,
        sorted: data.sorted,
        filtered: data.filtered
      }
    });

  dispatch('GENRES_LOADALL',  'genres',  res.Genres);
  dispatch('ARTISTS_LOADALL', 'artists', res.Artists);
  dispatch('ALBUMS_LOADALL',  'albums',  res.Albums);
  dispatch('TRACKS_LOADALL',  'tracks',  res.Tracks);

  socket.connect();
});

ReactDOM.render((
  <Provider store={store}>
    <ReduxRouter>
      {routes}
    </ReduxRouter>
  </Provider>
), document.getElementById('root'));
