import React from 'react';

export default function ProgressBar(props) {
  const { current, maximum } = props;
  const progress = (maximum === 0 ? 0 : ((current / maximum) * 100));
  const style = {
    width: `${progress}%`
  };

  return (
    <div className="progress">
      <div className="progress-bar" style={style} />
    </div>
  );
}
