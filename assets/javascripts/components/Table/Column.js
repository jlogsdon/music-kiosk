import { compose } from 'redux';
import React from 'react';

export default function Column(props) {
  const {
    // Header props
    label, property,
    // Passed when rendering as a Cell
    object, value,
    // Sorting props
    sortable, sortBy, sortDir,
    ...rest
  } = props;

  return <td {...rest}>{value}</td>;
}

Column.composeValue = getValue => compose(
  Column,
  props => ({
    ...props,
    value: getValue(props.value, props)
  })
);
