import moment from 'moment';
import Column from './Column';

export default function TimeAgoColumn(props) {
  return Column({
    ...props,
    value: moment(props.value).fromNow()
  });
}
