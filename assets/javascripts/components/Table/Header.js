import React from 'react';
import classNames from 'classnames';

export default function Header(props) {
  const {
    // Header props
    label, property,
    // Passed when rendering as a Cell
    object, value,
    // Sorting props
    sortable, sortBy, sortDir, toggleSort,
    ...rest
  } = props;

  function sortingAnchor() {
    const className = classNames({
      sortable: true,
      'sorting-asc':  (sortBy === property && sortDir === 'asc'),
      'sorting-desc': (sortBy === property && sortDir === 'desc')
    });

    return <a onClick={() => toggleSort(property)} className={className}>{label}</a>;
  }

  return (
    <th {...rest}>
      {sortable === false ? label : sortingAnchor()}
    </th>
  );
}
