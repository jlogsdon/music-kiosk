import React from 'react';
import classNames from 'classnames';

export default function LoadingIndicator(props){
  const { isLoading } = props;
  const className = classNames('preloader-wrapper', 'small', {
    active: isLoading
  });

  return (
    <div className={className}>
      <div className="spinner-layer spinner-red-only">
        <div className="circle-clipper left">
          <div className="circle"></div>
        </div>
        <div className="gap-patch">
          <div className="circle"></div>
        </div>
        <div className="circle-clipper right">
          <div className="circle"></div>
        </div>
      </div>
    </div>
  );
}
