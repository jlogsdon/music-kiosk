import React from 'react';
import { Link } from 'react-router';
import Icon from './Icon';

export default function IconLink(props) {
  const { to, href, icon, className, ...rest } = props;

  if (!to) {
    return (
      <a
        {...rest}
        href={href}
        className={`btn ${className}`}>
        <Icon>{icon}</Icon>
      </a>
    );
  } else {
    return (
      <Link
        {...rest}
        to={to}
        className={`btn ${className}`}>
        <Icon>{icon}</Icon>
      </Link>
    );
  }
}
