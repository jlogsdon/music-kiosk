import React, { Children } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames';

export default function TabView(props) {
  const { selected, children } = props;

  const tabs = children.filter(child => child.type === Tab);
  const pane = children.find(child => child.type !== Tab && child.key === selected);

  return (
    <div className="tab-view">
      <ol className="tabs">
        {tabs.map(tab =>
                  React.cloneElement(tab, {
                    key:    `tab_${tab.props.to}`,
                    active: selected === tab.props.to
                  }))}
      </ol>
      <section className={`pane ${selected}`}>
        {pane}
      </section>
    </div>
  );
}

export function Tab(props) {
  const {
    to,
    children,
    active
  } = props;

  const className = classNames({ active });

  return (
    <li className={className}>
      <Link to={to}>{children}</Link>
    </li>
  );
}
