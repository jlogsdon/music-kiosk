import React from 'react';
import { formatTime } from '../utils';

export default function NowPlaying(props) {
  const { mini, status, track, album, artist, position, id } = props;
  const { cover } = track;

  if (!track || !artist || !album || status !== 'playing') {
    return <div id={id}>Nothing Playing! You should pick a song...</div>;
  }

  const progress = (track.runtime === 0 ? 0 : ((position / track.runtime) * 100));
  const style = {
    width: `${progress}%`
  };

  if (mini) {
    return (
      <div className="now-playing">
        <img className="art" src={`${cover}/small`}/>
        <h4>Now Playing</h4>
        <div className="meta">
          <span className="artist">{artist.name}</span>
          <span className="song">{track.name}</span>
        </div>
        <div className="progress">
          <span className="bar">
            <span className="inner" style={style}/>
          </span>
        </div>
      </div>
    );
  }

  return (
    <section className="now-playing">
      <div className="widget">
        <header className="widget-head">
          <h2>Now Playing</h2>
        </header>
        <div className="widget-body">
          <img className="art" src={`${cover}/original`}/>
          <div className="info">
            <img className="bg" src={`${cover}/original`}/>
            <div className="progress">
              <span className="time-current">{formatTime(position)}</span>
              <span className="bar">
                <span className="inner" style={style}/>
              </span>
              <span className="time-total">{formatTime(track.runtime)}</span>
            </div>
            <div className="meta">
              <span className="song">{track.name}</span>
              <span className="artist">{artist.name} &mdash; {album.name}</span>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
