import React, { Component } from 'react';
import { Link } from 'react-router';
import classNames from 'classnames';

export default class ItemListLink extends Component {
  static contextTypes = {
    history: React.PropTypes.object
  };

  render() {
    const { children, to, onClick, selected } = this.props;
    const { history } = this.context;

    const className = classNames({
      selected,
      active: history.isActive(to)
    });

    return (
      <li className={className}>
        <Link to={to} onClick={onClick}>{children}</Link>
      </li>
    );
  }
}
