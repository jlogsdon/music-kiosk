import React from 'react';
import { Children } from 'react';

export default function Icon({ children }) {
  return <i className="material-icons">{children}</i>;
}
