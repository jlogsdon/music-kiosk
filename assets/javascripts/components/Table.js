import React, { Component, Children } from 'react';
import Header from './Table/Header'
import Column from './Table/Column'
import TimeAgoColumn from './Table/TimeAgoColumn'

const getValue = (object, property) => property.split('.').reduce((v, k) => v && v[k], object);

export { Header, Column, TimeAgoColumn };
export default class Table extends Component {
  static propTypes = {
    collection: React.PropTypes.array.isRequired,
    className:  React.PropTypes.string
  }

  constructor(...args) {
    super(...args);

    this.state = {
      sortBy:  this.props.defaultSortBy,
      sortDir: this.props.defaultSortDir || 'asc',
    };
  }

  render() {
    return (
      <table className="bordered striped">
        <thead>{this.renderHeaders()}</thead>
        <tbody>{this.renderRows()}</tbody>
      </table>
    );
  }
  renderHeaders() {
    const { children } = this.props;

    return (
      <tr>
      {Children.map(children, child =>
                          <Header
                            key={`header_${child.props.property}`}
                            sortBy={this.state.sortBy}
                            sortDir={this.state.sortDir}
                            toggleSort={this.toggleSort.bind(this)}
                            {...(child.props)}/>)}
      </tr>
    );
  }
  renderRows() {
    const { collection } = this.props;

    if (this.state.sortBy) {
      return collection.sort(this.sortItems.bind(this)).map(this.renderRow.bind(this));
    } else {
      return collection.map(this.renderRow.bind(this));
    }
  }
  renderRow(object, id) {
    const { children } = this.props;

    return (
      <tr key={`row_${id}`}>
      {Children.map(children, child => {
        const { cellType, property, ...rest } = child.props;
        const value = property && getValue(object, property);
        const key = `row_${id}_${property}`;

        return React.cloneElement(child, { value, object, key });
      })}
      </tr>
    );
  }

  toggleSort(property) {
    if (this.state.sortBy !== property) {
      this.setState({
        sortBy:  property,
        sortDir: 'asc'
      });
    } else {
      this.setState({
        sortDir: this.state.sortDir === 'asc' ? 'desc' : 'asc'
      });
    }
  }
  sortItems(a, b) {
    const { sortBy, sortDir } = this.state;
    const left  = getValue(a, sortBy);
    const right = getValue(b, sortBy);

    const result = (left > right) ? 1
                 : (left < right) ? -1
                 : 0;

    if (sortDir === 'asc') {
      return result;
    } else {
      return result * -1;
    }
  }
}
