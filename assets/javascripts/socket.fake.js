import { StatusActions } from './actions';
import store from './store';

const handlers = {
  'player-events:stopped':  StatusActions.stopped,
  'player-events:playing':  StatusActions.playing,
  'player-events:progress': StatusActions.progress
};
const getState = store.getState;

let tracks;

class SocketClient {
  connect() {
    StatusActions.connected();
    this.tick();
  }

  runtime() {
    return Math.round((+(new Date) - this.started) / 1000);
  }

  pickSong() {
    if (!tracks) {
      tracks = getState().Tracks.filtered;
    }
    if (!tracks || !tracks.length) {
      return;
    }

    const song = Math.floor(tracks.length * Math.random());
    this.playing = tracks[song];
    this.started = +(new Date);

    StatusActions.playing({
      position: this.runtime(),
      track: this.playing.id,
      status: 'playing'
    });
  }

  tick() {
    if (!this.playing) {
      this.pickSong();
    } else if (this.runtime() >= this.playing.runtime) {
      this.pickSong();
    } else {
      StatusActions.progress({
        position: this.runtime()
      });
    }

    setTimeout(this.tick.bind(this), 250);
  }
}

export default new SocketClient();
