import { createActionTypes } from '../utils';

module.exports = createActionTypes({
  Genres: ['loadAll'],
  Artists: ['loadAll', 'select'],
  Albums: ['loadAll', 'select'],
  Tracks: ['loadAll', 'select'],
  Status: ['connected', 'disconnected', 'playing', 'stopped', 'progress'],
  Browse: ['search', 'setQueue', '*save', '*getQueue', 'clearQueue', '*playNow'],
  Admin: [
    'login', 'logout', '*playNow', '*bumpUp', '*bumpDown', '*remove', '*rocket', '*skip',
    '*fetchAccessLists', '*saveAccessList', '*loadAccessList', '*clearAccessList'
  ]
});
