import superagent from 'superagent';
import { stringify } from 'qs';

['get', 'post', 'put', 'patch', 'del'].forEach(method => {
  request[method] = (...args) => request(method, ...args);
});

const formatUrl = path => `http://localhost:9292/${path}`;

export default function request(method, path, options) {
  return new Promise((resolve, reject) => {
    let request = superagent[method](formatUrl(path));

    if (options && options.params) {
      const query = typeof(options.params) === 'string' ? options.params : stringify(options.params);
      request.query(query);
    }
    if (options && options.data) {
      request.send(options.data);
    }

    request.end((err, res) => {
      if (err) {
        reject(err);
      } else {
        resolve(JSON.parse(res.text));
      }
    });
  });
}
