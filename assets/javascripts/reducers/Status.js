import { createReducer } from '../utils';
import { Status as StatusActionTypes } from '../constants/actions';

const initialState = {state: 'disconnected'};

export default createReducer(initialState, {
  [StatusActionTypes.connected] (state) {
    return {
      state: 'stopped'
    };
  },

  [StatusActionTypes.disconnected] (state) {
    return initialState;
  },

  [StatusActionTypes.stopped] (state) {
    return {
      state: 'stopped'
    };
  },

  [StatusActionTypes.playing] (state, { payload }) {
    return {
      ...state,
      state:    'playing',
      position: payload.position,
      track:    payload.track
    };
  },

  [StatusActionTypes.progress] (state, { payload }) {
    return {
      ...state,
      state:    'playing',
      position: payload.position
    };
  }
});
