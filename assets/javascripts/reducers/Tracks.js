import { assign, createReducer } from '../utils';
import {
  Artists as ArtistActionTypes,
  Albums as AlbumActionTypes,
  Tracks as TrackActionTypes,
  Browse as BrowseActionTypes
} from '../constants/actions';

const initialState = {
  tracks:  {},
  loading: false,

  selected: {},
  filter:   null,

  filtered: []
};

function filter({ sorted, tracks, filter, selected }) {
  if (selected.album) {
    return sorted.filter(id => tracks[id].album_id === selected.album);
  } else if (selected.artist) {
    return sorted.filter(id => tracks[id].artist_id === selected.artist);
  } else if (filter) {
    const searchTerm = filter.toLowerCase();
    return sorted.filter(id =>
      tracks[id].name.toLowerCase().indexOf(searchTerm) >= 0 ||
        tracks[id].album_name.toLowerCase().indexOf(searchTerm) >= 0 ||
          tracks[id].artist_name.toLowerCase().indexOf(searchTerm) >= 0
    );
  }

  return sorted;
}

export default createReducer(initialState, {
  [TrackActionTypes.loadAll] (state, { payload }) {
    return assign(initialState, {
      ...payload,
      loading: false
    });
  },

  [ArtistActionTypes.select] (state, action) {
    const newState = assign(state, {
      selected: assign(state.selected, {
        artist: action.payload
      })
    });

    return assign(newState, {
      filtered: filter(newState)
    });
  },

  [AlbumActionTypes.select] (state, action) {
    const newState = assign(state, {
      selected: assign(state.selected, {
        album: action.payload
      })
    });

    return assign(newState, {
      filtered: filter(newState)
    });
  },

  [BrowseActionTypes.search] (state, action) {
    const newState = assign(state, {
      filter: action.payload
    });

    return assign(newState, {
      filtered: filter(newState)
    });
  }
});
