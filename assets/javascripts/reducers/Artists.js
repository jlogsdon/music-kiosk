import { assign, createReducer } from '../utils';
import {
  Artists as ArtistActionTypes,
  Browse as BrowseActionTypes
} from '../constants/actions';

const initialState = {
  artists: {},
  loading: false,

  filter: null,

  filtered: [],
  sorted:   []
};

function filter({ sorted, artists, filter }) {
  if (filter) {
    const searchTerm = filter.toLowerCase();
    return sorted.filter(id => artists[id].name.toLowerCase().indexOf(searchTerm) >= 0);
  }

  return sorted;
}

export default createReducer(initialState, {
  [ArtistActionTypes.loadAll] (state, { payload }) {
    return assign(initialState, {
      ...payload,
      loading: false
    });
  },

  [BrowseActionTypes.search] (state, action) {
    const newState = assign(state, {
      filter: action.payload
    });

    return assign(newState, {
      filtered: filter(newState)
    });
  }
});
