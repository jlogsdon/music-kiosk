import { assign, createReducer } from '../utils';
import {
  Genres as GenreActionTypes
} from '../constants/actions';

const initialState = {
  genres: {},
  loading: false,

  sorted: [],
  filtered: []
};

export default createReducer(initialState, {
  [GenreActionTypes.loadAll] (state, { payload }) {
    return assign(initialState, {
      ...payload,
      loading: false
    });
  }
});
