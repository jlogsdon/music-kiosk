import { combineReducers } from 'redux';
import { routerStateReducer } from 'redux-react-router';
import Albums from './Albums';
import Artists from './Artists';
import Tracks from './Tracks';
import Status from './Status';
import Browse from './Browse';
import Admin from './Admin';
import Genres from './Genres';

export default combineReducers({
  Albums, Artists, Tracks, Status, Browse, Admin, Genres,
  router: routerStateReducer
});
