import { createReducer } from '../utils';
import { Browse as BrowseActionTypes } from '../constants/actions';

const initialState = {
  queue: [],
  persisted: [],
  loadingQueue: false
};

export default createReducer(initialState, {
  [BrowseActionTypes.setQueue] (state, action) {
    return {
      ...state,
      queue: action.payload
    };
  },

  [BrowseActionTypes.getQueue.PENDING] (state, action) {
    return {
      ...state,
      loadingQueue: true
    };
  },

  [BrowseActionTypes.getQueue.FULFILLED] (state, action) {
    return {
      ...state,
      persisted: action.payload,
      loadingQueue: false
    };
  },

  [BrowseActionTypes.getQueue.REJECTED] (state, action) {
    return {
      ...state,
      loadingQueue: false
    };
  },

  [BrowseActionTypes.clearQueue] (state, action) {
    return {
      ...state,
      queue: []
    };
  }
});
