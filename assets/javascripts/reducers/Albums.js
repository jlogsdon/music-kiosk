import { assign, createReducer } from '../utils';
import {
  Artists as ArtistActionTypes,
  Albums as AlbumActionTypes,
  Browse as BrowseActionTypes
} from '../constants/actions';

const initialState = {
  albums:  {},
  loading: true,

  selected: {},
  filter:   null,

  filtered: [],
  sorted:   []
};

function filter({ sorted, albums, filter, selected }) {
  if (selected.artist) {
    return sorted.filter(id => albums[id].artists.indexOf(selected.artist) >= 0);
  } else if (filter) {
    const searchTerm = filter.toLowerCase();
    return sorted.filter(id =>
      albums[id].name.toLowerCase().indexOf(searchTerm) >= 0 ||
        albums[id].artist_name.toLowerCase().indexOf(searchTerm) >= 0
    );
  }

  return sorted;
}

export default createReducer(initialState, {
  [AlbumActionTypes.loadAll] (state, { payload }) {
    return assign(initialState, {
      ...payload,
      loading: false
    });
  },

  [ArtistActionTypes.select] (state, action) {
    const newState = assign(state, {
      selected: assign(state.selected, {
        artist: action.payload
      })
    });

    return assign(newState, {
      filtered: filter(newState)
    });
  },

  [BrowseActionTypes.search] (state, action) {
    const newState = assign(state, {
      filter: action.payload
    });

    return assign(newState, {
      filtered: filter(newState)
    });
  }
});
