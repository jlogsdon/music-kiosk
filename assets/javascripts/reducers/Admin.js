import { assign, createReducer } from '../utils';
import {
  Admin as AdminActionTypes
} from '../constants/actions';

const initialState = {
  isAdmin: false,
  timeout: null,
  accessLists: []
};

export default createReducer(initialState, {
  [AdminActionTypes.login] (state, action) {
    return {
      ...state,
      isAdmin: true,
      timeout: action.payload
    };
  },

  [AdminActionTypes.logout] (state, action) {
    return {
      ...state,
      isAdmin: false,
      timeout: null
    };
  },

  [AdminActionTypes.fetchAccessLists.FULFILLED] (state, action) {
    return {
      ...state,
      accessLists: action.payload.map(list => ({
        id:      list.id,
        name:    list.name,
        allowed: list.list
      }))
    };
  }
});
