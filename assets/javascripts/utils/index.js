export { default as createActionTypes } from './createActionTypes';
export { default as createReducer } from './createReducer';
export { default as identFactory } from './identFactory';

export function isFunction(obj) {
  return typeof obj === 'function';
}

export function isPromise(obj) {
  return obj && isFunction(obj.then);
}

export function assign(to, ...rest) {
  return Object.assign({}, to, ...rest);
}

export function debounce(func, wait, immediate) {
  let timeout;
  return function(...args) {
    const context = this;
    const callNow = immediate && !timeout;

    const later = () => {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    clearTimeout(timeout);
    timeout = setTimeout(later, wait);

    if (callNow) {
      func.apply(context, args);
    }
  };
}

export function formatTime(timeInSeconds) {
  let minutes = Math.floor(timeInSeconds / 60),
      seconds = Math.round(timeInSeconds % 60).toString();

  if (seconds.length === 1) {
    seconds = `0${seconds}`;
  }

  return `${minutes}:${seconds}`;
}

export function formatNumber(num) {
  return num.toFixed(0).replace(/(\d)(?=(\d{3})+$)/g, '$1,');
}
