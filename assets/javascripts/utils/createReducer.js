export default function(initialState, handlers) {
  if (!handlers) {
    handlers = initialState;
    initialState = {};
  }

  return (state = initialState, action) => {
    const handler = handlers[action.type];
    if (!handler) {
      return state;
    } else {
      return handler(state, action);
    }
  };
}
