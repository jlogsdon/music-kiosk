import { isFunction } from '../utils';

export default function thunkMiddleware({ dispatch, getState }) {
  return next => function step(action) {
    if (isFunction(action)) {
      return next(action(getState, dispatch));
    }

    return next(action);
  };
}
