import { isPromise, identFactory } from '../utils';

const factories = {};
function getIdentifier(actionType) {
  const type = actionType.replace(/_[^_]+$/, '');

  if (!factories[type]) {
    factories[type] = identFactory(type);
  }

  return factories[type].next();
}

export default function promiseMiddleware({ dispatch, getState }) {
  return next => function step(action) {
    if (isPromise(action.payload)) {
      const {
        payload,
        type : { PENDING, FULFILLED, REJECTED },
        meta = {},
        ...rest
      } = action;

      meta.promiseIdentifier = getIdentifier(PENDING);

      next({ ...rest, meta, type: PENDING });
      return payload.then(
        result => next({ ...rest, meta, type: FULFILLED, payload: result }),
        error  => next({ ...rest, meta, type: REJECTED,  payload: error, error: true })
      ).catch(
        error  => next({ ...rest, meta, type: REJECTED,  payload: error, error: true })
      );
    }

    return next(action);
  };
}
