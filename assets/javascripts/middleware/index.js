import { createStore, applyMiddleware } from 'redux';
import loggerMiddleware from './logger';
import promiseMiddleware from './promise';
import thunkMiddleware from './thunk';

const middleware = [
  thunkMiddleware,
  promiseMiddleware,
  loggerMiddleware
];

export default applyMiddleware(...middleware)(createStore);
