const timeTracker = {};

function ignore(type) {
  if (type.indexOf('@@reduxReactRouter') === 0) {
    return true;
  }

  return false;
}

export default function loggerMiddleware() {
  return next => action => {
    const { meta = {}, type } = action;

    if (!(meta.silent || ignore(type))) {
      let descrip = type;

      if (meta.promiseIdentifier) {
        const [_, TYPE, STATE] = action.type.match(/(.*?)_(PENDING|FULFILLED|REJECTED)$/);
        const isTracked = Object.prototype.hasOwnProperty.call(timeTracker, meta.promiseIdentifier);

        descrip += ` (${meta.promiseIdentifier})`;

        if (STATE === 'PENDING') {
          timeTracker[meta.promiseIdentifier] = +(new Date);
        } else if (STATE && isTracked) {
          const timeTaken = (+(new Date) - timeTracker[meta.promiseIdentifier]) / 1000;
          descrip += ` (took ${timeTaken} seconds)`;
          delete timeTracker[meta.promiseIdentifier];
        }
      }

      console.groupCollapsed(descrip);
      console.dir(action);
      console.groupEnd(descrip);

      if (action.error) {
        console.error('Failed %s: %s', descrip, action.payload);
      }
    } else if (action.error) {
      console.error('Failed %s: %s', type, action.payload);
    }

    return next(action);
  };
}
