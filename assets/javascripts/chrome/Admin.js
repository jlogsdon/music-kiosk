import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { AdminActions } from '../actions';
import Main from './Main';
import Footer from './Footer';

class Home extends Component {
  componentWillMount() {
    AdminActions.fetchAccessLists();
  }

  render() {
    const { accessLists } = this.props;

    return (
      <section className="admin-home">
        <div className="widget">
          <header className="widget-head">
            <h3>
              <a className="btn panic" onClick={this.clearList}>Panic! Reset Active Whitelist!</a>
              Whitelist
            </h3>
          </header>

          <div className="widget-body">
            <ul>
            {accessLists.map((list, i) => (
              <li key={i}>
                <Link className="item" to={`/admin/access_lists/${list.id}`}>
                  {list.name}
                  <span className="btn" onClick={this.loadList(list.id)}>Load</span>
                </Link>
              </li>
            ))}
              <li>
                <Link to={`/admin/access_lists/new`}>Create a New Whitelist</Link>
              </li>
            </ul>
          </div>
        </div>
      </section>
    );
  }

  loadList(id) {
    return () => AdminActions.loadAccessList(id);
  }

  clearList() {
    AdminActions.clearAccessList();
  }
}
const ConnectedHome = connect(state => ({
  accessLists: state.Admin.accessLists
}))(Home);

function GenreEntry(props) {
  const { genre, present, removeAction, addAction } = props;

  const text = present ? 'Remove from List' : 'Add to List';
  const onClick = present ? removeAction : addAction;
  const button = <a className="btn" onClick={onClick}>{text}</a>;

  return (
    <li>
      {genre.name}
      {button}
    </li>
  );
}

class AccessList extends Component {
  constructor(props) {
    super(props);

    const accessList = (props.params && props.params.id) ?
      props.accessLists.find(list => list.id == props.params.id) : {
        name: 'New List',
        allowed: []
      };

    this.state = { ...accessList };
  }

  render() {
    const {
      genres,
      genresByID
    } = this.props;
    const { name, allowed } = this.state;

    return (
      <section className="admin-home pick-songs">
        <div className="widget">
          <header className="widget-head">
            <h3>
              {name}
              <a className="btn" onClick={this.changeName.bind(this)}>Change Name</a>
              <Link to="/admin" onClick={this.save.bind(this)}>Save List</Link>
            </h3>
          </header>

          <div className="widget-body pick-songs">
            <ul className="tab-content songs">
            {genres.map(id =>
                        <GenreEntry
                          key={id}
                          genre={genresByID[id]}
                          present={allowed.indexOf(id) >= 0}
                          removeAction={this.remove(id)}
                          addAction={this.add(id)}/>)}
            </ul>
          </div>
        </div>
      </section>
    );
  }

  remove(id) {
    return () => {
      this.setState({
        allowed: this.state.allowed.filter(_id => _id !== id)
      });
    };
  }

  add(id) {
    return () => {
      this.setState({
        allowed: (this.state.allowed || []).concat(id)
      });
    };
  }

  changeName() {
    const name = prompt('Enter a new name');

    if (name && name.length > 2) {
      this.setState({ name });
    }
  }

  save() {
    AdminActions.saveAccessList(this.state)
      .then(AdminActions.fetchAccessLists)
  }
}
const ConnectedAccessList = connect(state => ({
  accessLists: state.Admin.accessLists,
  genres: state.Genres.sorted,

  genresByID: state.Genres.genres
}))(AccessList);

class Admin extends Component {
  static contextTypes = {
    store: React.PropTypes.object
  }

  componentWillUpdate(nextProps) {
    const { history } = this.context.store;

    if (!nextProps.isAdmin) {
      history.pushState({}, '/');
    }
  }

  render() {
    const { children } = this.props;

    return (
      <div className="container">
        <header className="app-header">
          <h1>Admin</h1>
        </header>

        <Main className="with-header with-footer" id="admin">
          {children}
        </Main>

        <Footer>
          <div className="app-actions">
            <Link to="/" className="btn cancel">Leave</Link>
            <a className="btn cancel" onClick={AdminActions.logout}>Logout</a>
          </div>
        </Footer>
      </div>
    );
  }
}
const ConnectedAdmin = connect(state => ({
  isAdmin: state.Admin.isAdmin
}))(Admin);

ConnectedAdmin.Home = ConnectedHome;
ConnectedAdmin.AccessList = ConnectedAccessList;

export default ConnectedAdmin;
