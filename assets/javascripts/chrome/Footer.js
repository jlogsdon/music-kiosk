import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NowPlaying } from '../components';
import { nowPlaying } from '../selectors';

class Footer extends Component {
  render() {
    const { status, children } = this.props;

    return (
      <footer className="app-footer">
        <NowPlaying mini={true} {...status} />

        {children}
      </footer>
    );
  }
}
export default connect(state => ({
  status: nowPlaying(state),
}))(Footer);
