import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { AdminActions } from '../actions';

class Title extends Component {
  render() {
    const {
      children,
      isAdmin
    } = this.props;

    if (isAdmin) {
      return (
        <h1>
          <Link to="/admin">{children}</Link>
        </h1>
      );
    } else {
      return (
        <h1 onClick={this.checkAdmin.bind(this)}>
          {children}
        </h1>
      );
    }
  }

  checkAdmin() {
    if (this.props.admin) {
      return;
    }

    const password = prompt('Enter pass code for the powah');
    if (password) {
      AdminActions.attemptToLogin(password);
    }
  }
}
export default connect(state => ({
  isAdmin: state.Admin.isAdmin
}))(Title);
