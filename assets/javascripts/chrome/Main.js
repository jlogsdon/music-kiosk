import React, { Component } from 'react';
import { connect } from 'react-redux';
import { nowPlaying } from '../selectors';

class Main extends Component {
  render() {
    const { children, className, id, cover } = this.props;

    return (
      <main className={`app-main ${className}`} id={id}>
        <img src={`${cover}/original`} className="bg-texture"/>

        {children}
      </main>
    );
  }
}
export default connect(state => ({
  cover: nowPlaying(state).track.cover
}))(Main);
