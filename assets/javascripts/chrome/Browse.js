import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import classNames from 'classnames';
import {
  ArtistActions,
  AlbumActions,
  BrowseActions
} from '../actions';
import {
  ItemListLink,
  LazyRender
} from '../components';
import Footer from './Footer';
import Main from './Main';
import Title from './Title';
import { truncateFiltered } from '../selectors';
import {
  debounce,
  formatNumber,
  formatTime
} from '../utils';

function Tab(props) {
  const { to, icon, onClick, selected, children } = props;

  return (
    <ItemListLink to={to} onClick={onClick} selected={selected}>
      <i className={`tab-icon icon-${icon}`}/>
      {children}
      <span className="btn clear">
        <i className="icon-cross"/>
      </span>
    </ItemListLink>
  );
}

class Actions extends Component {
  submitQueue() {
    BrowseActions.save()
      .then(BrowseActions.getQueue);
  }
  rocketQueue() {
    BrowseActions.playNow()
      .then(BrowseActions.getQueue);
  }

  render() {
    const {
      queue, isAdmin, tab
    } = this.props;
    const className = classNames('btn queue i-left', {
      disabled: queue.length === 0,
      green:    queue.length > 0
    });

    const text = `Queue ${queue.length} songs`;
    const icon = queue.length === 0 ? 'blocked' : 'check';
    const onClick = (queue.length > 0) ? this.submitQueue : () => {};
    const buttonTo = tab === 'queue' ? '/' : '/browse/queue';

    return (
      <div className="app-actions">
        <Link to="/" className="btn cancel">Cancel</Link>
        {isAdmin && this.playNow()}

        <Link to="/" className={className} onClick={onClick}>
          <span className="icon">
            <i className={`icon-${icon}`} />
          </span>
          {text}
        </Link>
      </div>
    );
  }

  playNow() {
    const { queue } = this.props;
    const onClick = queue.length > 0 ? this.rocketQueue : () => {};
    const className = classNames('btn playNow', {
      disabled: queue.length === 0
    });

    return (
      <Link to="/" className={className} onClick={onClick}>
        Play Now
      </Link>
    );
  }
}
const ConnectedActions = connect(state => ({
  queue:   state.Browse.queue,
  isAdmin: state.Admin.isAdmin
}))(Actions);

class ArtistList extends Component {
  render() {
    const { items, artists } = this.props;

    return (
      <LazyRender maxHeight={600} className="tab-content artists">
        {items.map(id => (
          <li key={id}>
            <Link to="/browse/albums" onClick={() => ArtistActions.select(id)}>
              {artists[id].name}
              <i className="icon-next" />
            </Link>
          </li>
        ))}
      </LazyRender>
    );
  }
}
const ConnectedArtistList = connect(state => ({
  items: state.Artists.filtered,
  artists: state.Artists.artists
}))(ArtistList);

class AlbumList extends Component {
  render() {
    const { items, albums, isAdmin } = this.props;

    return (
      <ul className="albums tab-content">
        {items.slice(0, 750).map(id => (
          <li key={`album_${id}`}>
            <Link to="/browse/tracks" onClick={() => AlbumActions.select(id)}>
              <img className="art" src={`http://localhost:9292/cover/${id}/medium`}/>
              <span className="album">{albums[id].name}</span>
              <span className="artist">{albums[id].artist_name}</span>
            </Link>
            {isAdmin && this.albumAdd(id)}
          </li>
        ))}
      </ul>
    );
  }

  albumAdd(id) {
    return (
      <Link
        to="/"
        className="btn queueAll"
        onClick={() => BrowseActions.queueEntireAlbum(id)}>
        <span className="icon">
          <i className="icon-plus"/>
        </span>
      </Link>
    );
  }
}
const ConnectedAlbumList = connect(state => ({
  items:   state.Albums.filtered,
  albums:  state.Albums.albums,
  isAdmin: state.Admin.isAdmin
}))(AlbumList);

class TrackList extends Component {
  render() {
    const { items, tracks, queue, isAdmin } = this.props;

    return (
      <LazyRender maxHeight={600} className="songs tab-content">
        {items.slice(0, 10000).map(id => {
          const track = tracks[id];
          let action, className = '';

          if (queue.indexOf(track.id) >= 0) {
            action = () => BrowseActions.remove(track.id);
            className = 'selected';
          } else {
            action = () => BrowseActions.add(track.id);
          }

          return (
            <li key={id} className={className}>
              <a onClick={action}>
                <span className="song">{track.name}</span>
                <span className="time">{formatTime(track.runtime)}</span>
                <span className="artist">{track.artist_name}</span>
                <span className="album">{track.album_name}</span>
                <i className="icon-plus"/>
                <i className="icon-check"/>
              </a>
            </li>
          );
        })}
      </LazyRender>
    );
  }
}
const ConnectedTrackList = connect(state => ({
  items:   state.Tracks.filtered,
  tracks:  state.Tracks.tracks,
  queue:   state.Browse.queue
}))(TrackList);

class QueueList extends Component {
  render() {
    const { tracks, queue } = this.props;

    return (
      <ul className="songs tab-content">
        {queue.map(id => {
          const track = tracks[id];
          let action, className = '';

          if (queue.indexOf(track.id) >= 0) {
            action = () => BrowseActions.remove(track.id);
            className = 'selected';
          } else {
            action = () => BrowseActions.add(track.id);
          }

          return (
            <li key={id} className={className}>
              <a onClick={action}>
                <span className="song">{track.name}</span>
                <span className="time">{formatTime(track.runtime)}</span>
                <span className="artist">{track.artist_name}</span>
                <span className="album">{track.album_name}</span>
                <i className="icon-plus"/>
                <i className="icon-check"/>
              </a>
            </li>
          );
        })}
      </ul>
    );
  }
}
const ConnectedQueueList = connect(state => ({
  tracks: state.Tracks.tracks,
  queue:  state.Browse.queue
}))(QueueList);

class Browse extends Component {
  componentWillMount() {
    this.setSearchTermLater = debounce(this.setSearchTerm.bind(this), 150);
  }

  componentWillUnmount() {
    BrowseActions.search(null);
    ArtistActions.select(null);
    AlbumActions.select(null);
    BrowseActions.clearQueue();
  }

  setSearchTerm() {
    const searchTerm = this.refs.search.value;
    if (searchTerm.length >= 3) {
      BrowseActions.search(searchTerm);
    }
  }

  render() {
    const {
      artists, albums, tracks,
      selected,
      filter,
      isAdmin,
      params : { tab }
    } = this.props;
    const children = this.getChildren();

    const searchIconClasses = classNames('btn clear', {
      hide: !filter
    });

    return (
      <div className="container">
        <header className="app-header">
          <div className="search-box">
            <input
              type="text"
              ref="search"
              placeholder="Enter an artist, album or song"
              autofocus={true}
              onChange={this.setSearchTermLater}/>
            <a className={searchIconClasses} onClick={this.resetFilter('search')}>
              <i className="icon-cross"/>
            </a>
          </div>

          <Title>Search</Title>
        </header>

        <Main className="with-header with-footer" id="browse">
          <section className="pick-songs">
            <ul className="tabs">
              <Tab
                to="/browse/artists"
                icon="artists"
                onClick={this.resetFilter('artists')}
                selected={!!selected.artist}>
                <span className="tab-main">Artists</span>
                <span className="tab-meta">{(selected.artist && selected.artist.name) || formatNumber(artists)}</span>
              </Tab>
              <Tab
                to="/browse/albums"
                icon="albums"
                onClick={this.resetFilter('albums')}
                selected={!!selected.album}>
                <span className="tab-main">Albums</span>
                <span className="tab-meta">{(selected.album && selected.album.name) || formatNumber(albums)}</span>
              </Tab>
              <Tab
                to="/browse/tracks"
                icon="songs">
                <span className="tab-main">Songs</span>
                <span className="tab-meta">{formatNumber(tracks)}</span>
              </Tab>
            </ul>

            {children}
          </section>
        </Main>

        <Footer>
          <ConnectedActions tab={tab}/>
        </Footer>
      </div>
    );
  }

  getChildren() {
    const {
      params : { tab }
    } = this.props;

    switch (tab) {
      case 'queue':
        return <ConnectedQueueList />;
      case 'tracks':
        return <ConnectedTrackList />;
      case 'albums':
        return <ConnectedAlbumList />;
      default:
        return <ConnectedArtistList />;
    }
  }

  resetFilter(tab) {
    return () => {
      switch (tab) {
        case 'search':
          this.refs.search.value = '';
          BrowseActions.search(null);
          break;
        case 'artists':
          ArtistActions.select(null);
        case 'albums':
          AlbumActions.select(null);
      }
    };
  }
}
export default connect(state => ({
  artists:  state.Artists.filtered.length,
  albums:   state.Albums.filtered.length,
  tracks:   state.Tracks.filtered.length,
  selected: {
    artist: state.Artists.artists[state.Tracks.selected.artist],
    album:  state.Albums.albums[state.Tracks.selected.album]
  },
  filter:   state.Tracks.filter,
  isAdmin:  state.Admin.isAdmin
}))(Browse);
