import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import {
  AdminActions,
  BrowseActions
} from '../actions';
import NowPlaying from '../components/NowPlaying';
import Main from './Main';
import Title from './Title';
import {
  nowPlaying,
  upNext
} from '../selectors';

class Home extends React.Component {
  componentWillMount() {
    BrowseActions.getQueue();
  }

  render() {
    const { status, queue, isAdmin } = this.props;

    return (
      <div className="container">
        <header className="app-header">
          <Link to="/browse/artists" className="btn i-right">
            Request Songs
            <span className="icon">
              <i className="icon-arrow-right"/>
            </span>
          </Link>

          <Title>Choose Your Music Kiosk</Title>
        </header>

        <Main className="with-header" id="home">
          <NowPlaying {...status} />

          <section className="upcoming-queue">
            <div className="widget">
              <header className="widget-head">
                <h3>
                  Up Next
                  {isAdmin && this.skipButton()}
                </h3>
              </header>

              <div className="widget-body">
                <ul>
                {queue.map((item, i) => {
                  return (
                    <li key={i}>
                      <img className="art" src={`${item.track.cover}/original`}/>
                      <div className="meta">
                        <span className="song">{item.track.name}</span>
                        <span className="artist">{item.artist.name}</span>
                      </div>
                      {isAdmin && this.adminControls(item.id)}
                    </li>
                  );
                })}
                </ul>
              </div>
            </div>
          </section>
        </Main>
      </div>
    );
  }

  skipButton() {
    const refreshAfterPromise = (actionCreator) => {
      return () => {
        const action = actionCreator();
        if (action && action.then) {
          action.then(BrowseActions.getQueue);
        }
      };
    };
    const skip = refreshAfterPromise(() => AdminActions.skip());

    return (
      <a className="btn gray right" onClick={skip}>
        <span className="icon">
          <i className="icon-skip"/>
        </span>
      </a>
    );
  }

  adminControls(id) {
    const refreshAfterPromise = (actionCreator) => {
      return () => {
        const action = actionCreator();
        if (action && action.then) {
          action.then(BrowseActions.getQueue);
        }
      };
    };

    const playNow = refreshAfterPromise(() => AdminActions.rocket(id));
    const bumpUp = refreshAfterPromise(() => AdminActions.bumpUp(id));
    const bumpDown = refreshAfterPromise(() => AdminActions.bumpDown(id));
    const remove = refreshAfterPromise(() => AdminActions.remove(id));

    return (
      <div className="admin">
        <a className="btn gray" onClick={playNow}>
          <span className="icon">
            <i className="icon-play"/>
          </span>
        </a>
        <a className="btn gray" onClick={bumpUp}>
          <span className="icon">
            <i className="icon-arrow-up"/>
          </span>
        </a>
        <a className="btn gray" onClick={bumpDown}>
          <span className="icon">
            <i className="icon-arrow-down"/>
          </span>
        </a>
        <a className="btn gray" onClick={remove}>
          <span className="icon">
            <i className="icon-blocked"/>
          </span>
        </a>
      </div>
    );
  }
}
export default connect(state => ({
  status:  nowPlaying(state),
  queue:   upNext(state),
  isAdmin: state.Admin.isAdmin
}))(Home);
