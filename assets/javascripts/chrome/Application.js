import React, { Children } from 'react';
import { connect } from 'react-redux';

class Application extends React.Component {
  render() {
    const { loading, children, location } = this.props;

    if (loading) {
      return (
        <div className="container loader">
          <div className="cssload-loader-inner">
            <div className="cssload-cssload-loader-line-wrap-wrap">
              <div className="cssload-loader-line-wrap"/>
            </div>
            <div className="cssload-cssload-loader-line-wrap-wrap">
              <div className="cssload-loader-line-wrap"/>
            </div>
            <div className="cssload-cssload-loader-line-wrap-wrap">
              <div className="cssload-loader-line-wrap"/>
            </div>
            <div className="cssload-cssload-loader-line-wrap-wrap">
              <div className="cssload-loader-line-wrap"/>
            </div>
            <div className="cssload-cssload-loader-line-wrap-wrap">
              <div className="cssload-loader-line-wrap"/>
            </div>
          </div>
        </div>
      );
    }

    return Children.only(children);
  }
}

export default connect(state => ({
  loading: state.Artists.loading || state.Albums.loading || state.Tracks.loading,
  Status: state.Status
}))(Application);
