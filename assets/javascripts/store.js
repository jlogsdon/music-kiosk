import { compose } from 'redux';
import persistState, { mergePersistedState } from 'redux-localstorage';
import filter from 'redux-localstorage-filter';
import { reduxReactRouter } from 'redux-react-router';
import adapter from 'redux-localstorage/lib/adapters/localStorage';
import createHistory from 'history/lib/createHashHistory';
import createStoreWithMiddleware from './middleware';
import rootReducer from './reducers/index';

const reducer = compose(
)(rootReducer);

const storage = adapter(window.localStorage);

const finalCreateStore = compose(
  reduxReactRouter({ createHistory }),
)(createStoreWithMiddleware);

const store = finalCreateStore(reducer);

if (module.hot) {
  module.hot.accept('./reducers', () => {
    const nextRootReducer = require('./reducers/index');
    store.replaceReducer(nextRootReducer);
  });
}

window.Store = store;
export default store;
