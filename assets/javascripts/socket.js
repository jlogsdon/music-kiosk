import {
  BrowseActions,
  StatusActions
} from './actions';

const handlers = {
  'player-events:stopped':  StatusActions.stopped,
  'player-events:playing':  [StatusActions.playing, BrowseActions.getQueue],
  'player-events:progress': StatusActions.progress
};

const _socket = Symbol();
class SocketClient {
  get socketURL() {
    return 'ws://' + location.hostname + ':5300/socket';
  }

  connect() {
    if (this[_socket]) {
      return false;
    }

    const socket = this[_socket] = new WebSocket(this.socketURL);
    window.socket = socket;

    socket.onopen = StatusActions.connected;
    socket.onclose = () => {
      StatusActions.disconnected();
      this[_socket] = null;
      setTimeout(this.connect.bind(this), 5000);
    };
    socket.onmessage = (e) => {
      try {
        const {
          channel,
          payload : { event, payload }
        } = JSON.parse(e.data);

        const handler = handlers[`${channel}:${event}`];
        if (handler) {
          if (Array.isArray(handler)) {
            handler.forEach(h => h(payload));
          } else {
            handler(payload);
          }
        }
      } catch (e) {
        console.error('Failed to process message', e);
        console.log(e.stack);
        console.log(e.data);
      }
    };
  }
}

export default new SocketClient();
