import React from 'react';
import { Route, IndexRoute, Redirect } from 'react-router';
import store from './store';
import Application from './chrome/Application';
import Browse from './chrome/Browse';
import Home from './chrome/Home';
import Admin from './chrome/Admin';

function requireAdmin(nextState, redirectTo) {
  const { isAdmin } = store.getState().Admin;
  if (!isAdmin) {
    redirectTo('/');
  }
}

export default (
  <Route path="/" component={Application}>
    <Route path="browse/:tab" component={Browse}/>

    <Route path="admin" component={Admin} onEnter={requireAdmin}>
      <Route path="access_lists/new" component={Admin.AccessList} />
      <Route path="access_lists/:id" component={Admin.AccessList} />

      <IndexRoute component={Admin.Home} />
    </Route>

    <Redirect from="/browse" to="/browse/artists"/>
    <Redirect from="/" to="/browse/artists"/>
    <IndexRoute component={Home} />
  </Route>
);
