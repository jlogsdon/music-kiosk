function addScript(href) {
  var script = document.createElement('script');
  script.setAttribute('src', href);
  document.body.appendChild(script);
  return script;
}
function loadBundle() {
  addScript('/static/bundle.js');
}

if (!window.fetch) {
  addScript('http://localhost:9292/assets/fetch.js').onload = loadBundle;
} else {
  loadBundle();
}
