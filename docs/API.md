# API

There are two types of APIs provided by Round: Web and Socket. The former is for queue management
and genere whitelists for auto-play mode.

## Web API

## Socket API

The Socket API is currently exposed via a lightweight NodeJS server that shunts the message into
Redis for the Player or Queue services to consume.

### Player Commands

Player commands go through the `player-control` queue in Redis and control the Player Service.

#### `play`

Tell the player to start a specific `Track`

```
{"command": "play", "track": {"id": 123}}
```

#### `stop`

Forces the player to stop playing music. The queue will remain in its current state.

```
{"command": "stop"}
```

#### `skip`

Forces the player to skip the current track. This essentially stops the player and asks the queue
service to pick another song.

```
{"command": "stop"}
```

### Player Events

Player events go through the `player-events` queue in Redis and inform the frontend of state changes
in the Player Service.

#### `stopped`

Sent whenever the player has stopped streaming audio. Usually only triggered by the `stop` command, but may occur in
situations the Queue service is not able to return a track.

```json
{"event": "stopped"}
```

#### `playing`

When a track starts playing this event is sent with information about the track.

```json
{"event": "playing", "track": {"id": 123, "position": 0}}
```

#### `progress`

While a track is playing a progress tick is sent along.

```json
{"event": "progress", "position": 123}
```

### Queue Commands

Queue commands go through the `queue-control` queue in Redis and control the Queue Service.

#### `next`

Informs the Queue service we need the next track. When the service receives this command it will
trigger a `play` command if a new track is found.

```
{"command": "next"}
```
