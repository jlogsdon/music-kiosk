module EventEmitter
  def events
    @events ||= Hash.new
  end

  def on(event, callback=nil, &block)
    events[event.to_s] ||= []
    events[event.to_s] << (callback || block)
  end

  def trigger(event, *args)
    if events.key?(event.to_s)
      events[event.to_s].each { |handler| handler.call(*args) }
    end
  end
end
