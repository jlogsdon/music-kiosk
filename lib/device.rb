require_relative 'thread_runner'

class Device
  include ThreadRunner

  attr_reader :track

  def play(track)
    info("Starting #{track}")
    @track = Track[track['id']]
    play_file(@track.local_path)
  end
  def stop
    @track = nil
  end
  def track_complete
    @track = nil
    trigger(:complete)
  end

  def runtime
    !track.nil? && track.runtime
  end
  def now_playing
    !track.nil? && track.name
  end

  def process
    if runtime
      trigger(:progress, {
        position: position,
        runtime:  runtime
      })
    end

    sleep 1
    status_line
  end

  def status_line
    @ui_count ||= 1
    if (@ui_count -= 1) == 0
      @ui_count = 5
      info(info_line)
    end
  end
  def info_line
    if now_playing
      "Playing [#{format_playtime(position)}] #{now_playing}"
    else
      "Stopped..."
    end
  end

  def format_playtime(position)
    "#{format_time(position)}/#{format_time(runtime)}"
  end
  def format_time(time)
    minutes = time / 60
    seconds = time % 60
    "#{minutes}:#{seconds.to_s.rjust(2, '0')}"
  end
end

require_relative 'device/audite'
require_relative 'device/fake'
