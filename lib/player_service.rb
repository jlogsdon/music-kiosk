require_relative 'controller'
require_relative 'device'
require_relative 'interface'

class PlayerService
  def interface
    @interface ||= Interface.new
  end

  def controller
    @controller ||= Controller.new(interface)
  end

  def device
    @device ||= load_device_class.new(interface)
  end
  def device=(device)
    raise RuntimeError, 'cannot change the device while player is started' if started?
    @device = device
  end
  def load_device_class
    device_name = ENV.fetch('DEVICE') { default_device_name }
    Device.const_get(device_name)
  end
  def default_device_name
    'Fake'
  end

  def started?; @started == true; end
  def stopped?; not started?; end

  def start
    return if started?
    trap
    register
    @started = true

    interface.info("Starting All Threads")

    [
      controller.start,
      device.start,
      interface.start
    ].each(&:join)
  end
  def halt
    return if stopped?
    @started = false

    interface.info("Stopping All Threads")

    controller.halt
    device.halt
    interface.halt
  end

  def handle_play(payload)
    device.play(payload)
    handle_current_track(payload)
  end
  def handle_stop(_)
    device.stop
    Round.publish('player-events', {event: 'stopped'})
  end

  def handle_skip(_)
    device.stop
    Round.publish('queue-control', {command: 'next'})
  end

  def handle_current_track(_)
    if device.track
      Round.publish('player-events', {
        event: 'playing',
        payload: {
          track:    device.track.id,
          position: device.position
        }
      })
    else
      Round.publish('player-events', {event: 'stopped'})
    end
  end

  def handle_complete
    Round.publish('queue-control', {command: 'next'})
  end
  def handle_progress(payload)
    Round.publish('player-events', {
      event: 'progress',
      payload: payload
    })
  end

  protected
  def trap
    # Only allow traps to be set once
    return if @trapped
    @trapped = true
    at_exit { halt }
  end

  def register
    controller.on('play', method(:handle_play))
    controller.on('stop', method(:handle_stop))
    controller.on('skip', method(:handle_skip))
    controller.on('current_track', method(:handle_current_track))

    device.on('complete', method(:handle_complete))
    device.on('progress', method(:handle_progress))
    device.on('started',  method(:start_device))
  end

  def start_device
    sleep 0.1
    Round.publish('queue-control', {command: 'next'})
  end
end
