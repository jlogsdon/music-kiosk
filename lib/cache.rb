require 'singleton'

class Cache
  class MemoryStore
    def initialize(ttl: 0)
      @ttl   = ttl
      @store = {}
    end

    def cached?(key)
      entry = @store[key]
      return false if entry.nil?
      return true if @ttl == -1
      return Time.now < entry[:expires]
    end

    def write(key, value)
      @store[key] = {
        expires: Time.now + @ttl,
        value:   value
      }
    end

    def read(key)
      @store[key][:value]
    end

    def delete(key)
      @store.delete(key)
    end
  end
  class FileSystemStore
    def initialize(root, ttl: 0)
      @root = root
      @ttl  = ttl
    end

    def keygen(base)
      return -> (name) {
        key = base.(name)
        File.join(@root, key[0...2], key)
      }
    end

    def cached?(key)
      if File.exist?(key)
        return true if @ttl == -1

        age = Time.now - File.stat(key).mtime
        return age < @ttl
      else
        return false
      end
    end

    def write(key, value)
      serialized = Marshal.dump(value)
      FileUtils.mkdir_p(File.dirname(key))
      File.open(key, 'wb+') { |io| io << serialized }

      return value
    end

    def read(key)
      Marshal.load(File.read(key))
    end

    def delete(key)
      FileUtils.rm(key)
    end
  end
  class NullStore
    include Singleton

    def cached?(key)
      false
    end

    def write(key, value)
      value
    end

    def read(key)
      nil
    end

    def delete(key)
      value
    end
  end

  SHA1_KEYGEN = -> (name) { Digest::SHA1.hexdigest(name) }

  def initialize(store: NullStore.instance, keygen: SHA1_KEYGEN)
    @store  = store
    @keygen = keygen
  end

  def get(name)
    key = @keygen.(name)
    if @store.cached?(key)
      return @store.read(key)
    end

    result = yield
    unless result.nil?
      begin
        @store.write(key, result)
      rescue
      end
    end

    return result
  end

  def expire!(name)
    key = @keygen.(name)
    @store.delete(name)
  end
end
