module AccessListService
  QUEUE_NAME = 'access_lists'

  extend self

  # Prevent anyone from including this module, it should be used directly (via class methods)
  def self.extended(base)
    raise RuntimeError, 'do not include QueueService, use directly'
  end

  def read
    Oj.load(Round.redis.get(QUEUE_NAME) || '[]')
  end
  def write(list)
    Round.redis.set(QUEUE_NAME, Oj.dump(list))
  end

  # - item_id is the id of the type given
  def add(genre_id)
    list = read
    list << genre_id

    write(list)
  end

  # - type is either genre or artist
  # - item_id is the id of the type given
  def remove(genre_id)
    list = read
    list.delete(genre_id.to_s)

    write(list)
  end

  def clear
    write([])
  end

  def clear_invalid!
    write(read.reject { |id| Genre[id].nil? })
  end

  def scope
    query = Track

    list = read
    if list.any?
      query = query.where(genre_id: list)
    end

    return query
  end
end
