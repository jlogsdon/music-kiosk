require_relative 'event_emitter'
require_relative 'thread_runner'

class Controller
  include ThreadRunner

  def run
    Round.subscribe(:'player-control') do |on|
      on.message do |channel, message|
        handle_command(message)
      end
    end
  end

  def handle_command(raw)
    message = Oj.load(raw)

    command = message['command']
    payload = message['payload']

    trigger(command, payload)
  rescue Oj::Error => e
    error("Failed to parse command '#{raw}': #{e.message}")
  end
end
