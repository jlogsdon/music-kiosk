require 'ox'

class ItunesParser
  def self.plist_to_obj_xml(xml)
    xml = xml.gsub(%{<plist version="1.0">
}, '')

    xml.gsub!(%{
</plist>}, '')

    { '<dict>' => '<h>',
      '</dict>' => '</h>',
      '<dict/>' => '<h/>',
      '<array>' => '<a>',
      '</array>' => '</a>',
      '<array/>' => '<a/>',
      '<string>' => '<s>',
      '</string>' => '</s>',
      '<string/>' => '<s/>',
      '<key>' => '<s>',
      '</key>' => '</s>',
      '<integer>' => '<i>',
      '</integer>' => '</i>',
      '<date>' => '<s>',
      '</date>' => '</s>',
      '<data>' => '<s>',
      '</data>' => '</s>',
      '<integer/>' => '<i/>',
      '<real>' => '<f>',
      '</real>' => '</f>',
      '<real/>' => '<f/>',
      '<true/>' => '<y/>',
      '<false/>' => '<n/>',
    }.each do |pat,rep|
      xml.gsub!(pat, rep)
    end

    xml
  end

  def self.parse(xml)
    xml = benchmark('gsub xml') { plist_to_obj_xml(xml) }
    benchmark('parse xml') { ::Ox.load(xml, :mode => :object) }
  end

  def self.benchmark(message)
    $stderr.print "#{message}..."

    start = Time.now
    result = yield
    ending = Time.now

    $stderr.puts "#{message} took #{ending - start} seconds!"

    return result
  end
end
