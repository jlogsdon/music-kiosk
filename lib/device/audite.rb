class Device::Audite < Device
  def start
    init_audite!
    super
  end

  def init_audite!
    @buffer_size = 2 ** 14

    require 'audite'
    @device = ::Audite.new(@buffer_size)

    @device.events.on(:complete) { track_complete }
  end

  def stop
    @device.stop_stream
    super
  end

  def position
    @device.position.to_i
  end

  protected
  def play_file(file)
    debug("Playing file #{file}")
    begin
      @device.load(file)
    rescue => e
      $stderr.puts e.message
      sleep 0.1
      return track_complete
    end
    @device.start_stream unless @device.active
  end
end
