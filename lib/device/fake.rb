class Device::Fake < Device
  def process
    if runtime && position >= runtime
      track_complete
    end

    super
  end

  def position
    return 0 if @started_at.nil?
    (Time.now - @started_at).round
  end

  def stop
    @started_at = nil
  end

  def play_file(file)
    @started_at = Time.now
  end
end
