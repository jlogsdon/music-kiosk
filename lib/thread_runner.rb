require_relative 'event_emitter'

module ThreadRunner
  include EventEmitter

  def initialize(interface)
    @interface = interface
  end

  def program; self.class.name; end

  def start
    @started = true
    info("Starting")
    @worker = Thread.new { run }
    trigger(:started)
    return @worker
  end
  def halt
    @started = false
    info("Halting")
    @worker.kill
  end

  def started?; @started == true; end
  def stopped?; not started?; end

  def run
    process while started?
  rescue => e
    fatal(e.message)
    debug(e.backtrace.join("\n"))
    raise e
  end

  def process
    sleep 0.1
  end

  def debug(message);   @interface.debug(message, program); end
  def info(message);    @interface.info(message, program); end
  def warn(message);    @interface.warn(message, program); end
  def error(message);   @interface.error(message, program); end
  def fatal(message);   @interface.fatal(message, program); end
  def unknown(message); @interface.unknown(message, program); end
end
