require_relative 'access_list_service'
require_relative 'interface'
require_relative 'thread_runner'

class QueueService
  def interface
    @interface ||= Interface.new
  end
  def handler
    @handler ||= QueueHandler.new(interface)
  end

  def started?; @started == true; end
  def stopped?; not started?; end

  def start
    return if started?
    trap
    @started = true

    interface.info("Starting All Threads")

    [
      handler.start,
      interface.start
    ].each(&:join)
  end
  def halt
    return if stopped?
    @started = false

    interface.info("Stopping All Threads")

    handler.halt
    interface.halt
  end

  protected
  def trap
    # Only allow traps to be set once
    return if @trapped
    @trapped = true
    at_exit { halt }
  end
end
class QueueHandler
  QUEUE_NAME = 'queue:main'

  include ThreadRunner

  def run
    Round.subscribe(:'queue-control') do |on|
      on.message do |channel, message|
        handle_command(message)
      end
    end
  end

  def handle_command(raw)
    message = Oj.load(raw)

    command = message['command']
    payload = message['payload']
    puts "Got #{raw}"

    case command
    when 'next' then pick_next
    end
  rescue => e
    error("Failed to parse command '#{raw}': #{e.message}")
    debug(e.backtrace.join("\n"))
  end

  def pick_next
    track = add_history(next_from_queue || pick_random)

    Round.publish('player-control', {
      command: 'play',
      payload: track
    })
  end
  def all
    Round.redis.lrange(QUEUE_NAME, 0, -1).map { |id| Selection[id] }
  end
  def add(selection)
    Round.redis.rpush(QUEUE_NAME, selection.id.to_s)
  end
  def addTop(selection)
    Round.redis.lpush(QUEUE_NAME, selection.id.to_s)
  end
  def remove(selection)
    Round.redis.lrem(QUEUE_NAME, 0, selection.id.to_s)
  end
  def clear
    Round.redis.del(QUEUE_NAME)
  end

  def rocket(selection)
    ids = Round.redis.lrange(QUEUE_NAME, 0, -1)
    idx = ids.index(selection.id.to_s)

    return if idx == 0

    remove(selection)
    addTop(selection)
  end
  def moveUp(selection)
    ids = Round.redis.lrange(QUEUE_NAME, 0, -1)
    idx = ids.index(selection.id.to_s)

    return if idx == 0

    Round.redis.lset(QUEUE_NAME, idx - 1, ids[idx])
    Round.redis.lset(QUEUE_NAME, idx, ids[idx - 1])
  end
  def moveDown(selection)
    ids = Round.redis.lrange(QUEUE_NAME, 0, -1)
    idx = ids.index(selection.id.to_s)

    return if idx == ids.length - 1

    Round.redis.lset(QUEUE_NAME, idx + 1, ids[idx])
    Round.redis.lset(QUEUE_NAME, idx, ids[idx + 1])
  end

  def next_from_queue
    while next_id = Round.redis.lpop(QUEUE_NAME)
      debug("Next from queue: #{next_id}")
      @selection = Selection[next_id]
      return @selection.track unless @selection.nil?
    end
  end

  def pick_random
    query = AccessListService.scope
    debug("Picking Random")

    offset = 1 + (rand * (query.count) - 1).round
    query.where('filename NOT LIKE ?', '%.m4a').offset(offset).first
  end

  def add_history(track)
    data = {track_id: track.id, played_at: Time.now}
    unless @selection.nil?
      data[:selection_id] = @selection.id
      @selection = nil
    end
    info("Selected #{track.id}")
    History.create(data)
    return track
  end
end
