require 'set'

class Exporter < Struct.new(:target)
  def self.call(target)
    new(target).call
  end

  class Collection
    attr_reader :records

    def initialize
      @records = {}
      @sorted  = []
      @filtered = @sorted
    end

    def get(item)
      @records[item['id']] ||= visit(build(item))
    end

    def build(item)
      item
    end

    def visit(item)
      @sorted.push(item['id'])
      return item
    end

    def complete
      @records.each do |_, record|
        record.each do |k,v|
          next unless v.is_a?(Set)
          record[k] = v.to_a
        end
      end
    end

    def as_json
      {
        records:  @records,
        sorted:   @sorted,
        filtered: @filtered
      }
    end
  end

  class GenreCollection < Collection
    def add(item)
      get(item['genre'])
    end
  end
  class ArtistCollection < Collection
    def build(artist)
      artist.merge(
        albums: Set.new,
        tracks: Set.new
      )
    end

    def add(item)
      artist = get(item['artist'])

      artist[:albums].add(item['album']['id'])
      artist[:tracks].add(item['track']['id'])
    end
  end
  class AlbumCollection < Collection
    def build(album)
      album.merge(
        artists: Set.new,
        tracks:  Set.new
      )
    end

    def add(item)
      album = get(item['album'])

      album[:artists].add(item['artist']['id'])
      album[:tracks].add(item['track']['id'])
      album[:artist_name] = item['artist']['name']
    end
  end
  class TrackCollection < Collection
    def add(item)
      track = get(item['track'])

      track[:genre_id]  = item['genre']['id']
      track[:artist_id] = item['artist']['id']
      track[:album_id]  = item['album']['id']
      track[:genre_name]  = item['genre']['name']
      track[:artist_name] = item['artist']['name']
      track[:album_name]  = item['album']['name']
    end
  end

  class Store
    attr_reader :genres, :artists, :albums, :tracks

    def initialize
      @genres = GenreCollection.new
      @artists = ArtistCollection.new
      @albums = AlbumCollection.new
      @tracks = TrackCollection.new
      @all = [ @genres, @artists, @albums, @tracks ]
    end

    def add(track)
      @all.each { |c| c.add(track) }
    end

    def complete
      @all.each(&:complete)
    end

    def as_json
      {
        'Genres'  => @genres.as_json,
        'Artists' => @artists.as_json,
        'Albums'  => @albums.as_json,
        'Tracks'  => @tracks.as_json
      }
    end
  end

  def call
    scope = Track.paging
    progress = ProgressBar.new(scope.count)
    store = Store.new

    query_as_json(scope) do |track, i|
      store.add(track)

      progress.increment!
    end

    store.complete

    File.open(target, 'w+') do |io|
      io << JSON.generate(store.as_json)
    end
  end

  def query_as_json(query)
    result = Sequel::DATABASES[0].execute(query.sql)
    columns = result.columns

    result.map do |row|
      columns.each_with_object({}) do |column, obj|
        if column.include?('__')
          child, column = column.split('__', 2)
          obj[child] ||= {}
          obj[child][column] = row.shift
        else
          obj[column] = row.shift
        end
      end.tap { |m| yield(m) }
    end
  end
end
