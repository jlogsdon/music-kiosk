require_relative 'config/application'
require 'progress_bar'
require 'taglib'

task :pry do
  Round.init
  require 'pry'
  binding.pry
end

task :player do
  require_relative 'lib/player_service'
  Round.init
  PlayerService.new.start
end

task :queue do
  require_relative 'lib/queue_service'
  Round.init
  QueueService.new.start
end

namespace :import do
  task :itunes do
    library_file = ENV.fetch('LIBRARY_FILE') { raise 'LIBRARY_FILE is required' }
    library_root = ENV.fetch('LIBRARY_ROOT') { raise 'LIBRARY_ROOT is required' }
    library_name = ENV.fetch('LIBRARY_NAME') { raise 'LIBRARY_NAME is required' }

    Round.init

    source = Source.where(name: library_name).first
    source ||= Source.create(name: library_name, root: library_root)

    require_relative 'lib/itunes_parser'
    require_relative 'lib/importer/track'

    plist = ItunesParser.parse(File.read(library_file))
    bar = ProgressBar.new(plist['Tracks'].length)

    plist['Tracks'].each do |_, track|
      bar.increment!
      next if track['Location'].blank?

      if Importer::Track.call(track, source)
        true
      else
        "Unable to save track for #{track['Location'].to_s}"
      end
    end
  end

  task :covers do
    Round.init

    albums = Album.where(has_cover: false)
    bar = ProgressBar.new(albums.count)
    size = 0
    missing = 0

    cover_root = File.expand_path('./assets/covers', __dir__)

    albums.each do |album|
      cover_file = (100_000_000 * album.id).to_s(36)
      cover_folder = File.join(cover_root, cover_file[0...2])
      cover_path = File.join(cover_folder, cover_file)

      album.tracks.each do |track|
        next unless track.filename =~ /\.mp3$/

        cover = track.get_cover
        next if cover.nil?

        mime_type = MimeMagic.by_magic(cover)
        next if mime_type.nil?

        cover_file << ".#{mime_type.subtype}"

        `mkdir -p #{cover_folder}` unless Dir.exist?(cover_folder)
        File.open(cover_path, 'w+') { |io| io << cover }

        size += cover.bytesize
        album.has_cover = true
        album.save
        break
      end

      missing += 1 unless album.has_cover

      bar.increment!
    end

    puts "Total size: #{size}"
    puts "Missing art: #{missing}"
  end
end

task :export do
  require 'json'
  require_relative 'lib/exporter'
  Round.init

  Exporter.call(ENV['TARGET'])
end

task :fix_times do
  Round.init

  tracks = Track.all
  bar = ProgressBar.new(tracks.count)

  tracks.each do |track|
    track.runtime = track.runtime / 1000
    track.save
    bar.increment!
  end
end

task :clean_bad_tracks do
  Round.init

  tracks = Track.where('filename LIKE ?', '%.m4a')
  tracks.each do |track|
    mp3_path = track.local_path.gsub(/m4a$/, 'mp3').gsub(/Janus\/iTunes/, 'Janus/Converted')
    mp3_fold = File.dirname(mp3_path)

    if File.exist?(mp3_path)
      track.filename = mp3_path
      track.save
    end
  end
end

task :convert do
  Round.init

  tracks = Track.where('filename LIKE ?', '%.m4a').all.to_a
  bar = ProgressBar.new(tracks.count)

  done = File.read('converted.txt').split("\n")

  tracks.each do |track|
    if done.include?(track.local_path)
      bar.increment!
      next
    end

    mp3_path = track.local_path.gsub(/m4a$/, 'mp3').gsub(/Janus\/iTunes/, 'Janus/Converted')
    mp3_fold = File.dirname(mp3_path)

    if !Dir.exist?(mp3_fold)
      `mkdir -p "#{mp3_fold}"`
    elsif File.exist?(mp3_path)
      Open3.capture3('rm', mp3_path)
    end

    cmd = [
      'ffmpeg',
      '-i', track.local_path,
      '-codec:v', 'copy',
      '-codec:a', 'libmp3lame',
      '-q:a', '2',
      mp3_path
    ]
    std_out, std_err, status = Open3.capture3(*cmd)

    File.open('converted.txt', 'a') do |io|
      io.puts track.local_path
    end

    unless status.success?
      puts "Failed #{std_err}"
      break
    end

    bar.increment!
  end
end

task :verify do
  Round.init

  tracks  = Track.all
  bar     = ProgressBar.new(tracks.count)
  folders = Hash.new { |hash, key| hash[key] = Set.new }

  tracks.each do |track|
    folder = File.dirname(track.local_path)
    folders[folder].add(track.album_id)
    bar.increment!
  end

  folders.each do |folder, albums|
    next if albums.length < 2

    puts "#{albums.length} albums in #{folder}"
  end
end
