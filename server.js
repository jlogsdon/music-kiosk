var fs = require('fs');
var ws = require('nodejs-websocket');
var http = require('http');
var Redis = require('redis');

var redisSub = Redis.createClient(5000, 'localhost');
var redisPub = Redis.createClient(5000, 'localhost');

function broadcast(message) {
  socketServer.connections.forEach(function(connection) {
    connection.sendText(message);
  });
}
redisSub.on('message', function(channel, message) {
  try {
    var payload = JSON.parse(message);
  } catch (e) {
    console.log('invalid message received for ' + channel, message);
    return;
  }

  broadcast(JSON.stringify({
    channel: channel,
    payload: JSON.parse(message)
  }));
});
redisSub.subscribe('player-events');

var idFactory = (function IdentFactory(name) {
  var counter = 0;
  this.next = function() {
    return name + '_' + (++counter);
  };

  return this;
})('Connection');

var socketServer = ws.createServer(function(conn) {
  var id = idFactory.next();
  console.log(id, 'opened');

  conn.on('text', function(str) {
    console.log(id, 'command:', str);
    redisPub.publish('player-control', str);
  });
  conn.on('error', function(err) {
    console.log(id, 'error', err);
  });
  conn.on('close', function() {
    console.log(id, 'closed');
  });

  redisPub.publish('player-control', JSON.stringify({command: 'current_track'}));
});

socketServer.listen(process.env.PORT);
console.log('Waiting for connections on port ' + process.env.PORT + '...');
