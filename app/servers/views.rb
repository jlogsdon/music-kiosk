require_relative '../../lib/asset_builder'

class ViewServer < Sinatra::Base
  get '/favicon.ico' do
    status 404
  end

  get '/assets/:file.css' do |file|
    content_type 'text/css'

    return AssetBuilder.scss(File.join(APP_ROOT, 'assets', 'stylesheets', "#{file}.scss"))
  end
  get '/assets/:file.js' do |file|
    content_type 'text/javascript'

    return File.read(File.join(APP_ROOT, 'assets', "#{file}.js"))
  end
  get '/assets/:file.map' do |file|
    content_type 'text/javascript'

    return File.read(File.join(APP_ROOT, 'assets', "#{file}.map"))
  end

  get '/assets/fonts/:font' do |font|
    filepath = File.join(APP_ROOT, 'assets', 'stylesheets', 'fonts', font)
    puts filepath

    if File.exist?(filepath)
      return File.read(filepath)
    else
      halt(404)
    end
  end

  get '/cover/:id/:size' do |id, size|
    name = (100_000_000 * id.to_i).to_s(36)
    filepath = File.join(APP_ROOT, 'assets', 'covers', name[0...2], name)
    original = filepath.dup

    if File.exist?(original)
      case size
      when 'small', 'medium'
        filepath << ".#{size}"
      end
    else
      filepath = File.join(APP_ROOT, 'assets', 'covers', '_blank')
      original = "#{filepath.dup}.jpg"

      if %w(small medium).include?(size)
        filepath << ".#{size}"
      end

      filepath << '.jpg'
    end

    if !File.exist?(filepath)
      dimensions = case size
                   when 'small' then '42x42'
                   when 'medium' then '350x350'
                   end

      system('convert', original, '-resize', dimensions, filepath)
    end

    cover = File.read(filepath)

    mime_type = MimeMagic.by_magic(cover)
    content_type mime_type.type

    cover
  end

  get '/data.json' do
    content_type 'application/json'
    file_path = File.join(APP_ROOT, 'public', 'data.json')

    if File.exist?(file_path)
      File.read(file_path)
    else
      halt(404)
    end
  end
  get '/data/:type.json' do |type|
    content_type 'application/json'
    file_path = File.join(APP_ROOT, 'public', 'data', "#{type}.json")

    if File.exist?(file_path)
      File.read(file_path)
    else
      halt(404)
    end
  end

  get /.*/ do
    File.read(File.join(APP_ROOT, 'public', 'index.html'))
  end

  private
  def random_cover
    File.join(APP_ROOT, 'assets', 'covers', '_blank.jpg')
  end
end
