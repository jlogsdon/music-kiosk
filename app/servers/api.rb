require 'sinatra/json'

require_relative '../../lib/access_list_service'
require_relative '../../lib/queue_service'

class ApiServer < Sinatra::Base
  helpers Sinatra::JSON

  before do
    content_type 'text/json', :charset => 'utf-8'

    if request.content_type =~ /\Aapplication\/json/
      request.body.rewind
      body = request.body.read.to_s

      if body
        begin
          params.merge!(Oj.load(body))
        rescue Oj::Error => e
          puts "Failed to parse JSON body: #{e.message}"
        end
      end
    end
  end

  def cache(*args, &block)
    key = Oj.dump(args)
    Round.cache.get(key, &block)
  end

  def paginate(scope, maybe_page, per_page: 1_000)
    pages = (scope.count / per_page.to_f).ceil
    page  = (maybe_page || 1).to_i

    to_json = Proc.new do |query|
      result = Sequel::DATABASES[0].execute(query.sql)
      columns = result.columns

      result.map do |row|
        columns.each_with_object({}) do |column, obj|
          if column.include?('__')
            child, column = column.split('__', 2)
            obj[child] ||= {}
            obj[child][column] = row.shift
          else
            obj[column] = row.shift
          end
        end
      end
    end

    if page < 1 || page > pages
      status(404)
      json(error: 'Page not found')
    else
      paging = {
        pages: pages,
        page:  page,
        items: scope.count
      }

      base_url, query = request.url.split('?', 2)
      query = CGI::parse(query || '')
      page_url = -> (page) { "#{base_url}?#{URI.encode_www_form(query.merge('page' => page))}" }

      if page > 1
        paging[:first]    = base_url
        paging[:previous] = page_url.(page - 1)
      end
      if page < pages
        paging[:next] = page_url.(page + 1)
        paging[:last] = page_url.(pages)
      end

      query = scope
        .offset(per_page * (page - 1))
        .limit(per_page)

      json({
        paging: paging,
        items: to_json.(query)
      })
    end
  end
  def paging_info(scope, per_page: 1_000)
    {
      items: scope.count,
      pages: (scope.count / per_page.to_f).ceil
    }
  end

  get '/tracks' do
    cache('tracks', params[:page]) do
      paginate(Track.paging, params[:page], per_page: 5_000)
    end
  end

  get '/queue' do
    service = QueueHandler.new(logger)

    json(service.all.map(&:as_json))
  end
  post '/queue' do
    service = QueueHandler.new(logger)

    params['ids'].each do |id|
      if id.is_a?(Array)
        id = id.last
      end

      track = Track[id]
      next if track.nil?

      selection = Selection.create(
        track_id: id,
        queued_at: Time.now,
        requested_by: params['name']
      )
      service.add(selection)
    end

    json status: 'OK'
  end

  get '/history' do
    json(History.order(Sequel.desc(:played_at)).limit(50).map { |o| o.as_json(deep: true) })
  end

  # Queue a track at the top of the list and then skip the current track so it plays immediately!
  post '/admin/play' do
    service = QueueHandler.new(logger)

    params['ids'].reverse.each do |track_id|
      track = Track[track_id]
      next if track.nil?

      selection = Selection.create(
        track_id: track.id,
        queued_at: Time.now,
        requested_by: params['name']
      )

      service.addTop(selection)
    end

    Round.publish('player-control', {
      command: 'skip'
    })

    json status: 'OK'
  end

  post '/admin/skip' do
    Round.publish('player-control', {
      command: 'skip'
    })

    json status: 'OK'
  end

  post '/admin/queue/:id/up' do |id|
    service = QueueHandler.new(logger)
    selection = Selection[id]
    halt(404) if selection.nil?

    service.moveUp(selection)

    json status: 'OK'
  end
  post '/admin/queue/:id/down' do |id|
    service = QueueHandler.new(logger)
    selection = Selection[id]
    halt(404) if selection.nil?

    service.moveDown(selection)

    json status: 'OK'
  end
  post '/admin/queue/:id/play' do |id|
    service = QueueHandler.new(logger)
    selection = Selection[id]
    halt(404) if selection.nil?

    service.rocket(selection)
    Round.publish('player-control', {
      command: 'skip'
    })

    json status: 'OK'
  end
  delete '/admin/queue/:id' do |id|
    service = QueueHandler.new(logger)
    selection = Selection[id]
    halt(404) if selection.nil?

    service.remove(selection)

    json status: 'OK'
  end

  get '/admin/access_lists' do
    json AccessList.all.map(&:as_json)
  end
  post '/admin/access_lists' do
    if params['name'].nil?
      halt(422)
    else
      access_list = (AccessList[params['id']] || AccessList.new)

      access_list.name = params['name']
      access_list.list = params['allowed']
      access_list.save

      json status: 'OK'
    end
  end
  post '/admin/access_lists/clear' do
    AccessListService.clear

    json status: 'OK'
  end

  post '/admin/access_lists/:id/load' do |id|
    list = AccessList[id]

    if list.nil?
      halt(404)
    else
      AccessListService.write(list.list)

      json status: 'OK'
    end
  end
end
