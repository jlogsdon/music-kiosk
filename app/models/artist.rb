class Artist < Sequel::Model
  one_to_many :tracks
  many_to_many :genres

  def to_s
    self.name
  end

  def as_json(options={})
    {
      id:   self.id,
      name: self.name
    }
  end

  def self.for_init
    self.order(:sort_name, :name)
  end

  def self.paging
    self
      .order(:sort_name, :name)
      .select(:id, :name)
  end
end
