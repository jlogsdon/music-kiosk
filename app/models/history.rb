class History < Sequel::Model
  many_to_one :track
  many_to_one :selection

  def as_json(options={})
    json = {
      id:           self.id,
      track_id:     self.track_id,
      selection_id: self.selection_id,
      played_at:    self.played_at
    }

    if options[:deep] && !selection.nil?
      json[:selection] = selection.as_json(deep: false)
    end

    return json
  end
end
