class Album < Sequel::Model
  many_to_one :artist
  one_to_many :tracks

  def as_json(options={})
    {
      id:   self.id,
      name: self.name
    }
  end

  def self.for_init
    self.order(:sort_name, :name).eager(:tracks)
  end

  def self.paging
    self.select(:id, :name, :compilation)
  end
end
