class Track < Sequel::Model
  many_to_one :source
  many_to_one :genre
  many_to_one :artist
  many_to_one :album
  one_to_many :histories
  one_to_many :selections

  def to_s
    "#{artist} - #{self.name}"
  end

  def local_path
    if self.filename =~ /file:\/\//
      URI.decode(self.filename.gsub(%r{file://.*?iTunes}, source.root))
    else
      File.join(source.root, self.filename)
    end
  end

  def as_json(options={})
    {
      id:        self.id,
      genre:     self.genre_id,
      artist:    self.artist_id,
      album:     self.album_id,
      name:      self.name,
      runtime:   self.runtime
    }
  end

  def get_cover
    return unless filename =~ /\.mp3$/

    TagLib::MPEG::File.open(local_path) do |mp3_file|
      tag = mp3_file.id3v2_tag
      return if tag.nil?

      cover = tag.frame_list('APIC').first
      return if cover.nil?

      cover.picture
    end
  end

  def self.paging
    map = Proc.new do |table, field|
      Sequel.as("#{table}s__#{field}".to_sym, "#{table}__#{field}".to_sym)
    end

    self
      .join(:genres, id: :tracks__genre_id)
      .join(:artists, id: :tracks__artist_id)
      .join(:albums, id: :tracks__album_id)
      .order(
        Sequel.asc(Sequel.function(:lower, Sequel.qualify(:artists, :name))),
        Sequel.asc(Sequel.function(:lower, Sequel.qualify(:albums, :name))),
        Sequel.asc(:tracks__track_num)
      )
      .select(
        map.(:track, :id), map.(:track, :name),
        map.(:track, :track_num), map.(:track, :runtime),
        map.(:genre, :id), map.(:genre, :name),
        map.(:artist, :id), map.(:artist, :name),
        map.(:album, :id), map.(:album, :name)
      )
  end
end
