require 'rubygems'
require 'bundler'
require 'logger'
Bundler.require(:default)

Dotenv.load

APP_ROOT = File.expand_path('..', __dir__)
Sequel.extension :core_extensions
Oj.default_options = {mode: :compat}

module Round
  extend self

  def database
    @database ||= Sequel.connect(self.database_url, logger: Logger.new('log/queries.log'))
  end
  def database_url
    @database_url ||= ENV.fetch('DATABASE_URL', 'sqlite://db/round.db')
  end

  def cache
    return @cache if defined?(@cache)

    require_relative '../lib/cache'
    @cache = Cache.new(store: cache_store)
  end
  def cache_store
    @cache_store ||= Cache::MemoryStore.new(ttl: -1)
  end

  def redis
    @redis ||= create_redis_client
  end
  def publish(channel, payload)
    redis.publish(channel, Oj.dump(payload))
  end

  def subscribe(channel)
    @subscriber ||= create_redis_client
    @subscriber.subscribe(channel) { |on| yield(on) }
  end

  def redis_url
    @redis_url ||= URI.parse(ENV.fetch('REDIS_URL', 'redis://localhost:5000'))
  end
  def create_redis_client
    Redis.new(host: redis_url.host, port: redis_url.port, password: redis_url.password)
  end

  def init
    self.database

    require_relative '../app/models/access_list'
    require_relative '../app/models/source'
    require_relative '../app/models/genre'
    require_relative '../app/models/artist'
    require_relative '../app/models/artist_genre'
    require_relative '../app/models/album'
    require_relative '../app/models/track'
    require_relative '../app/models/history'
    require_relative '../app/models/selection'
  end
end
