require_relative 'config/application'
require 'rack/cors'
require 'oj'

Round.init
Oj.mimic_JSON

class Rack::Profiler < Struct.new(:app)
  def profile
    RubyProf.start
    result = yield
    profile = RubyProf.stop

    printer = RubyProf::FlatPrinter.new(profile)
    report  = File.join('profiles', Time.now.to_i.to_s)
    File.open(report, 'w+') { |io| printer.print(io) }

    return result
  end

  def call(env)
    profile { self.app.call(env) }
  end
end

map '/api' do
  use Rack::Profiler if ENV['PROFILE'] == 'on'
  use Rack::Deflater
  use Rack::Cors do
    allow do
      origins '*'
      resource '*', :headers => :any, :methods => [:get, :post, :put, :delete, :options, :head]
    end
  end

  require_relative 'app/servers/api'
  run ApiServer
end
map '/' do
  use Rack::Deflater
  use Rack::Cors do
    allow do
      origins '*'
      resource '*', :headers => :any, :methods => [:get, :post, :put, :delete, :options, :head]
    end
  end

  require_relative 'app/servers/views'
  run ViewServer
end
